#include "division_test.h"
#include "arithmetique.h"
#ifdef HAVE_CONFIG_H
  #include "config.h"
#endif

#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include <limits>

// Enregistrer la classe de test dans le registre de la suite
CPPUNIT_TEST_SUITE_REGISTRATION(divisionTest);

void divisionTest::setUp() {}

void divisionTest::tearDown() {}

/* TODO: Ajouter les définitions des méthodes de test de la classe
 * divisionTest
 */
void divisionTest::division_normal()
{
  operandeA = 9;
  operandeB = 3;
  CPPUNIT_ASSERT_EQUAL(static_cast<long int>(3),
                       static_cast<long int>(arithmetique::division(operandeA, operandeB))
    );
  operandeB = -3;
  CPPUNIT_ASSERT_EQUAL(static_cast<long int>(-3),
                       static_cast<long int>(arithmetique::division(operandeA, operandeB))
    );
}

void divisionTest::division_max()
{
  operandeA = std::numeric_limits<int>::max();
  operandeB = 1;
  CPPUNIT_ASSERT_GREATER(static_cast<long int>(operandeA),
                         static_cast<long int>(arithmetique::division(operandeA, operandeB))
    );
}

void divisionTest::division_min()
{
  operandeA = std::numeric_limits<int>::lowest();
  operandeB = -1;
  CPPUNIT_ASSERT_LESS(static_cast<long int>(operandeA),
                      static_cast<long int>(arithmetique:division(operandeA, operandeB))
    );
}

void divisionTest::division_zero()
{
  operandeA = 0;  operandeB = 1;
  CPPUNIT_ASSERT_EQUAL(static_cast<long int>(0),
                       static_cast<long int>(arithmetique::division(operandeA, operandeB))
    );
  CPPUNIT_ASSERT_EQUAL(static_cast<>(),
                       static_cast<long int>(arithmetique::division(operandeB, operandeA))
    );
}

int main()
{
  // Obtenir le registre de tests
  CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry();
  // Obtenir la suite principale depuis le registre
  CppUnit::Test *suite = registry.makeTest();
  // Ajouter la suite à l'exécutant des tests
  CppUnit::TextUi::TestRunner runner;
  runner.addTest(suite);
  // Définir l'utilitaire de sortie au format "erreurs de compilation"
  runner.setOutputter(new CppUnit::CompilerOutputter(&runner.result(),
                      std::cerr));
  // Lancer l'exécution des tests
  bool wasSucessful = runner.run();
  // Retourner le code d'erreur 1 si l'un des tests a échoué
  return wasSucessful ? 0 : 1;
}
